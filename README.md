# Wordpress Quick Install custom

## based on WP Quick Install 1.4.2

![capture](wp-quick-install_screenshot.png)

Ajout de deux champs à la version originale
* L'adresse du site pour modification de la base
  ![capture](wp-quick-install_url_screenshot.png)
* La possibilité de supprimer les thèmes par défaut
  ![capture](wp-quick-install_themes_screenshot.png)
* Seul le fichier index.php a été modifié, et le répertoire custom a été ajouté

## Mode d'emploi
* Modifier les paramètres dans custom/Config.php
* Modifier le context dans index.php (retirer proxy)
* Mettre le theme souhaité sous le nom theme.zip à la racine de wp-quick-install (au même niveau que index.php)
* **Attention** le Delete the content ne supprime que le contenu d'exemple, pas toute la base


## Modification en cas de mise à jour de wp-quick-install

### index.php
* Remplacer

  <pre><code>update_option( 'siteurl', $url );
  update_option( 'home', $url );</code></pre>

  par

  <pre><code>if( isset( $_POST['site_url'] ) && ! empty( $_POST['site_url'] ) ) {
    update_option( 'siteurl', $_POST['site_url'] );
    update_option( 'home', $_POST['site_url'] );
  } else {
    update_option( 'siteurl', $url );
    update_option( 'home', $url );
  }</code></pre>

* Ajouter en dessous des scripts
  <pre><code>&lt;?php
    include_once 'custom/custom.php';
    echo '&lt;script type=""&gt;' . "\n";
    echo $script;
    echo '&lt;/script&gt;' . "\n";
  ?&gt;</code></pre>

* Ajouter <code>include_once 'custom/tools.php';</code>
  en dessous de <code>require( 'inc/functions.php' );</code>

* Ajouter
  <pre><code>if ( $_POST['delete_default_plugins'] ) == '1' ) {
  rrmdir($directory . '/wp-content/plugins');
  mkdir($directory . '/wp-content/plugins');
  }</code></pre>

  sous
  <pre><code>unlink( $directory . '/wp-content/plugins/hello.php' );</code></pre>
  (**Attention** ne fonctionne que si l'on vide la base ou que les extensions ainsi supprimées n'étaient pas présentes avant)
