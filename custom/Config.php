<?php
class Config {
  //Paramètres base de donnée
  public $dbname           = 'wordpress_test';
  public $uname            = 'root';
  public $pwd              = '';
  public $dbhost           = 'localhost';
  public $prefix           = 'wp_';
  public $default_content  = 'true';

  //Paramètres généraux
  public $language        = 'Français';
  public $directory       = '../wp_quick';
  public $weblog_title    = 'Test Site';
  public $site_url        = 'http://localhost/wp_quick';
  public $user_login      = 'admin';
  public $admin_password  = 'pSPReg2g08$';
  public $admin_email     = 'test@test.test';
  public $blog_public     = 'true';

  //Paramètres du thème
  public $activate_theme  = 'true';
  public $delete_default_themes  = 'true';

  //Paramètres des extensions
  public $plugins           = 'contact-form-7';
  public $plugins_premium   = 'false';
  public $activate_plugins  = 'true';
  public $delete_default_plugins = 'true';

  //Paramètres de liens
  public $permalink_structure  = '/%postname%/';

  //Paramètres de média
  public $thumbnail_size_w               = '0';
  public $thumbnail_size_h               = '0';
  public $thumbnail_crop                 = 'true';
  public $medium_size_w                  = '0';
  public $medium_size_h                  = '0';
  public $large_size_w                   = '0';
  public $large_size_h                   = '0';
  public $upload_dir                     = '';
  public $uploads_use_yearmonth_folders  = 'true';

  //Paramètres du wp_config.php
  public $post_revisions      = '0';
  public $disallow_file_edit  = 'true';
  public $autosave_interval   = '7200';
  public $debug               = 'false';
  public $wpcom_api_key       = '0';

  // Paramètres hors saisie
  //public $validation_auto = 'window.onload = function() {document.querySelector("#submit").click();}'; //validation automatique: oui
  public $validation_auto = ''; //validation automatique: non
}
?>
