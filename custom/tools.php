<?php

function rrmdir($dir) {
    foreach(scandir($dir) as $file) {
        if ('.' === $file || '..' === $file) continue;
        if (is_dir("$dir/$file")) rrmdir("$dir/$file");
        else unlink("$dir/$file");
    }
    rmdir($dir);
}
 
?>
