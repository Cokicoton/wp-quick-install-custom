<?php
include_once 'Config.php';

$script = file_get_contents('custom/script.js');
$config = new Config();

foreach($config as $attribut => $valeur){
  $script = str_replace('$$' . $attribut . '$$', $valeur, $script);
}

?>
