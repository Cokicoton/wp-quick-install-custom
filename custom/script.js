//Insertion de site_url
var tr = document.createElement('tr');
var th = document.createElement('th');
th.setAttribute('scope', 'row');
var label = document.createElement('label');
label.setAttribute('for', 'site_url');
label.textContent = 'Site Url';
th.appendChild(label);
tr.appendChild(th);
var td = document.createElement('td');
var input = document.createElement('input');
input.setAttribute('name','site_url');
input.setAttribute('type','text');
input.setAttribute('size','25');
input.setAttribute('value','');
input.setAttribute('class','required');
input.id = 'site_url';
td.appendChild(input);
tr.appendChild(td);
document.querySelector('body > form > table:nth-child(9) > tbody').insertBefore(tr, document.querySelector('body > form > table:nth-child(9) > tbody > tr:nth-child(4)'));

//Insertion de delete_default_plugins
var tr = document.createElement('tr');
var th = document.createElement('th');
th.setAttribute('scope', 'row');
var label = document.createElement('label');
label.setAttribute('for', 'delete_default_plugins');
label.textContent = 'Default Plugins';
th.appendChild(label);
tr.appendChild(th);
var td = document.createElement('td');
var label2 = document.createElement('label2');
var input = document.createElement('input');
input.setAttribute('name','delete_default_plugins');
input.setAttribute('type','checkbox');
input.setAttribute('value','1');
input.id = 'delete_default_plugins';
label2.appendChild(input);
label2.innerHTML = label2.innerHTML + " Delete the default extensions (Askimet).";
td.appendChild(label2);
tr.appendChild(td);
document.querySelector('body > form > table:nth-child(16) > tbody').appendChild(tr);

//Paramètres base de donnée
document.querySelector('#dbname').value = '$$dbname$$';
document.querySelector('#uname').value = '$$uname$$';
document.querySelector('#pwd').value = '$$pwd$$';
document.querySelector('#dbhost').value = '$$dbhost$$';
document.querySelector('#prefix').value = '$$prefix$$';
document.querySelector('#default_content').checked = $$default_content$$;

//Paramètres généraux
var languages = document.querySelectorAll('#language option');
for(var i=0; i<languages.length; i++){
  if(languages[i].textContent == '$$language$$'){
    languages[i].selected = true;
  } else {
    languages[i].selected = false;
  }
}
document.querySelector('#directory').value = '$$directory$$';
document.querySelector('#weblog_title').value = '$$weblog_title$$';
document.querySelector('#site_url').value = '$$site_url$$';
document.querySelector('#user_login').value = '$$user_login$$';
document.querySelector('#admin_password').value = '$$admin_password$$';
document.querySelector('#admin_email').value = '$$admin_email$$';
document.querySelector('#blog_public').checked = $$blog_public$$;

//Paramètres du thème
document.querySelector('#activate_theme').checked = $$activate_theme$$;
document.querySelector('#delete_default_themes').checked = $$delete_default_themes$$;

//Paramètres des extensions
document.querySelector('#plugins').value = '$$plugins$$';
document.querySelector('#plugins_premium').checked = $$plugins_premium$$;
document.querySelector('#activate_plugins').checked = $$activate_plugins$$;
document.querySelector('#delete_default_plugins').checked = $$delete_default_plugins$$;

//Paramètres de liens
document.querySelector('#permalink_structure').value = '$$permalink_structure$$';

//Paramètres de média
document.querySelector('#thumbnail_size_w').value = '$$thumbnail_size_w$$';
document.querySelector('#thumbnail_size_h').value = '$$thumbnail_size_h$$';
document.querySelector('#thumbnail_crop').checked = $$thumbnail_crop$$;
document.querySelector('#medium_size_w').value = '$$medium_size_w$$';
document.querySelector('#medium_size_h').value = '$$medium_size_h$$';
document.querySelector('#large_size_w').value = '$$large_size_w$$';
document.querySelector('#large_size_h').value = '$$large_size_h$$';
document.querySelector('#upload_dir').value = '$$upload_dir$$';
document.querySelector('#uploads_use_yearmonth_folders').checked = $$uploads_use_yearmonth_folders$$;

//Paramètres du wp_config.php
document.querySelector('#post_revisions').value = '$$post_revisions$$';
document.querySelector('#disallow_file_edit').checked = $$disallow_file_edit$$;
document.querySelector('#autosave_interval').value = '$$autosave_interval$$';
document.querySelector('#debug').checked = $$debug$$;
document.querySelector('#wpcom_api_key').value = '$$wpcom_api_key$$';

//Validation auto
$$validation_auto$$

